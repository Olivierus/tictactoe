﻿using UnityEngine.SceneManagement;
using Unity.Networking.Transport;
using Unity.Collections;
using UnityEngine;
using System.Linq;

public class ClientBehaviour : Connection
{
    private NetworkConnection m_Connection;

    private void Start()
    {
        m_Driver = new UdpNetworkDriver(new INetworkParameter[0]);
        m_Connection = default(NetworkConnection);

        NetworkEndPoint endpoint = NetworkEndPoint.Parse(IP, 9000);
        m_Connection = m_Driver.Connect(endpoint);

        StartCoroutine(RemoveData());
    }

    private void Update()
    {
        m_Driver.ScheduleUpdate().Complete();

        print("Packets waiting: " + allData.Count);

        if (!m_Connection.IsCreated)
        {
            Debug.Log("Something went wrong during connect");
            return;
        }

        DataStreamReader stream;
        NetworkEvent.Type cmd;

        if (allData.Count > 0)
        {
            foreach (object data in allData.ToList())
            {
                SendData(data);
            }
        }

        while ((cmd = m_Connection.PopEvent(m_Driver, out stream)) != NetworkEvent.Type.Empty)
        {
            if (cmd == NetworkEvent.Type.Connect)
            {
                StartGame(false);
            }
            else if (cmd == NetworkEvent.Type.Data)
            {
                var readerCtx = default(DataStreamReader.Context);
                ReceiveData(stream, readerCtx);
            }
            else if (cmd == NetworkEvent.Type.Disconnect)
            {
                Debug.Log("Client got disconnected from server");
                m_Connection = default(NetworkConnection);
                SceneManager.LoadScene("Lobby");
            }
        }
    }

    private void SendData(object data)
    {
        byte[] bytes = DataHelper.CreateData(data);
        using (DataStreamWriter writer = new DataStreamWriter(bytes.Length, Allocator.Temp))
        {
            writer.Write(bytes);
            m_Connection.Send(m_Driver, writer);
        }
    }
}
