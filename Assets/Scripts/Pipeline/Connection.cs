using System.Collections;
using System.Collections.Generic;
using Unity.Networking.Transport;
using UnityEngine;

public abstract class Connection : MonoBehaviour
{
    public UdpNetworkDriver m_Driver;
    public List<object> allData = new List<object>();
    [HideInInspector]
    public string IP;

    private static Connection _instance;

    public static Connection instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void OnDestroy()
    {
        CleanUp();
    }

    private void OnApplicationQuite()
    {
        CleanUp();
    }

    protected virtual void CleanUp()
    {
        FindObjectOfType<RemoveLobby>().DeactivatedLobby();
        m_Driver.Dispose();
        NetworkUtilities.ClosePortInFirewall();
    }

    protected void StartGame(bool server)
    {

        GameObject go = new GameObject("GameManager", typeof(GameManager));

        if (server)
        {
            Debug.Log("We are now connected to a Server");
            go.GetComponent<GameManager>().StartGame(server);
        }
        else
        {
            Debug.Log("We are now connected to a Client");
            go.GetComponent<GameManager>().StartGame(server);
        }

        Destroy(GameObject.Find("UI_Qeue"));
    }

    protected void ReceiveData(DataStreamReader stream, DataStreamReader.Context readerCtx)
    {
        Packet data = DataHelper.ReceiveData(readerCtx, stream);
        EventManager.TriggerEvent(data.eventName, (object)data.data);
    }

    protected IEnumerator RemoveData()
    {
        yield return new WaitForSeconds(5);
        allData.Clear();
        StartCoroutine(RemoveData());
    }
}