using System.Runtime.Serialization.Formatters.Binary;
using Unity.Networking.Transport;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;

public static class DataHelper
{
    public static byte[] CreateData(object data)
    {
        byte[] bytes = ObjectToBytes(data);
        byte[] amountOfBytes = new byte[4];
        amountOfBytes = BitConverter.GetBytes(bytes.Length);
        return new List<byte>().Concat(amountOfBytes).Concat(bytes).ToArray();
    }

    public static dynamic ReceiveData(DataStreamReader.Context readerCtx, DataStreamReader stream)
    {
        int amountOfBytes = GetAmountOfBytes(readerCtx, stream);
        byte[] bytes = stream.ReadBytesAsArray(ref readerCtx, 4 + amountOfBytes);
        return BytesToObject(bytes.Skip(4).ToArray());
    }

    private static byte[] ObjectToBytes(object data)
    {
        if (data == null)
            return null;

        DataWrapper wraptData = new DataWrapper(data, data.GetType());
        BinaryFormatter binaryFormater = new BinaryFormatter();

        using (MemoryStream memoryStream = new MemoryStream())
        {
            binaryFormater.Serialize(memoryStream, wraptData);
            return memoryStream.ToArray();
        }
    }

    private static dynamic BytesToObject(byte[] bytes)
    {
        if (bytes == null)
            return null;

        BinaryFormatter binaryFormater = new BinaryFormatter();
        using (MemoryStream memoryStream = new MemoryStream(bytes))
        {
            DataWrapper data = (DataWrapper)binaryFormater.Deserialize(memoryStream);
            return Convert.ChangeType(data.value, data.type);
        }
    }

    private static int GetAmountOfBytes(DataStreamReader.Context readerCtx, DataStreamReader stream)
    {
        byte[] byteSize = new byte[4];

        for (int i = 0; i < 4; i++)
        {
            byteSize[i] = stream.ReadByte(ref readerCtx);
        }
        return BitConverter.ToInt32(byteSize, 0);
    }
}