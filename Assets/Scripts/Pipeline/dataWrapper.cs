using System;

[Serializable]
public struct DataWrapper
{
    public object value;
    public Type type;

    public DataWrapper(object value, Type type)
    {
        this.value = value;
        this.type = type;
    }
}