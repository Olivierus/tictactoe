﻿using UnityEngine.SceneManagement;
using Unity.Networking.Transport;
using UnityEngine.Assertions;
using Unity.Collections;
using UnityEngine;
using System.Linq;

public class ServerBehaviour : Connection
{
    private NativeList<NetworkConnection> m_Connections;

    private void Start()
    {
        m_Driver = new UdpNetworkDriver(new INetworkParameter[0]);
        if (m_Driver.Bind(NetworkEndPoint.Parse(NetworkUtilities.GetLocalIPAddress(), 9000)) != 0)
            Debug.Log("Failed to bind to port 9000");
        else
            m_Driver.Listen();

        m_Connections = new NativeList<NetworkConnection>(1, Allocator.Persistent);

        StartCoroutine(RemoveData());
    }

    protected override void CleanUp()
    {
        base.CleanUp();
        m_Connections.Dispose();
    }

    private void Update()
    {
        m_Driver.ScheduleUpdate().Complete();

        print("Packets waiting: " + allData.Count);

        for (int i = 0; i < m_Connections.Length; i++)
        {
            if (!m_Connections[i].IsCreated)
            {
                m_Connections.RemoveAtSwapBack(i);
                --i;
            }
        }

        NetworkConnection c;
        while ((c = m_Driver.Accept()) != default(NetworkConnection))
        {
            m_Connections.Add(c);
            StartGame(true);
        }

        DataStreamReader stream;
        for (int i = 0; i < m_Connections.Length; i++)
        {
            if (!m_Connections[i].IsCreated)
                Assert.IsTrue(true);

            if (allData.Count > 0)
            {
                foreach (object data in allData.ToList())
                {
                    SendData(data);
                }
            }

            NetworkEvent.Type cmd;
            while ((cmd = m_Driver.PopEventForConnection(m_Connections[i], out stream)) != NetworkEvent.Type.Empty)
            {
                if (cmd == NetworkEvent.Type.Data)
                {
                    var readerCtx = default(DataStreamReader.Context);
                    ReceiveData(stream, readerCtx);
                }
                else if (cmd == NetworkEvent.Type.Disconnect)
                {
                    Debug.Log("Client disconnected from server");
                    m_Connections[i] = default(NetworkConnection);
                    SceneManager.LoadScene("Lobby");
                }
            }
        }
    }

    private void SendData(object data)
    {
        byte[] bytes = DataHelper.CreateData(data);
        using (DataStreamWriter writer = new DataStreamWriter(bytes.Length, Allocator.Temp))
        {
            writer.Write(bytes);
            for (int i = 0; i < m_Connections.Length; i++)
            {
                m_Connections[i].Send(m_Driver, writer);
            }
        }
    }
}
