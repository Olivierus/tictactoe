﻿using UnityEngine;

public class RemoveLobby : MonoBehaviour
{
    private QueryHandler<loginData> qh;
    private bool done;

    private void OnApplicationQuit()
    {
        DeactivatedLobby();
    }

    public void DeactivatedLobby()
    {
        string userIP = NetworkUtilities.GetLocalIPAddress();

        string[] queryArguments = { "LobbyName=" + PlayerPrefs.GetString("LobbyName") };

        qh = new QueryHandler<loginData>("RemoveLobby", queryArguments);

        StartCoroutine(qh.HandleServerRequest());

        Destroy(FindObjectOfType<Connection>().gameObject);
        Destroy(this.gameObject);
    }
}