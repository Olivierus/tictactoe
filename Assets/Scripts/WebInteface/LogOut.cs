using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class LogOut : MonoBehaviour
{
    private QueryHandler<loginData> qh;
    private bool done;
    private Lobby lobby;

    private void OnApplicationQuit()
    {
        string userIP = NetworkUtilities.GetLocalIPAddress();
        string[] queryArguments = {
            "SessionID=" + PlayerPrefs.GetString("session_id")};

        qh = new QueryHandler<loginData>("Logout", queryArguments);

        StartCoroutine(qh.HandleServerRequest());
    }
}