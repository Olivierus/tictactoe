﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour
{
    private string password;
    private string username;

    private QueryHandler<loginData> qh;
    private bool done = false;

    public void OnEnter()
    {
        password = GameObject.Find("Password").transform.GetComponent<InputField>().text;
        username = GameObject.Find("Username").transform.GetComponent<InputField>().text;

        string[] queryArguments = {"Username="+username,
                                    "Password="+password};

        qh = new QueryHandler<loginData>("SignIn", queryArguments);

        StartCoroutine(qh.HandleServerRequest());
    }

    private void Update()
    {
        if (qh == null) { return; }

        loginData result;
        qh.RequestDone(out result);
        if (!done)
            HandleResult(result, GameObject.Find("Error").GetComponent<Text>());
    }

    private void HandleResult(loginData result, Text output)
    {
        PlayerPrefs.SetString("session_id", result.SSID);

        if (result.Response == "-1")
        {
            //login action
            Debug.Log("Logged in");
            done = true;
            DontDestroyOnLoad(new GameObject("HandleLogout", typeof(LogOut)));
            NetworkUtilities.OpenPortInFirewall();
            SceneManager.LoadScene("Lobby");
        }
        else
        { output.text = result.Response; }
    }
}

[System.Serializable]
public struct loginData
{
    public string SSID;
    public string Response;
}
