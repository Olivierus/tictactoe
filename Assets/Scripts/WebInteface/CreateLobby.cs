﻿using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class CreateLobby : MonoBehaviour
{
    private string lobbyName;
    private QueryHandler<loginData> qh;
    private bool done;

    public void Create()
    {
        lobbyName = GameObject.Find("NewLobbyName").transform.GetComponent<InputField>().text;

        string userIP = NetworkUtilities.GetLocalIPAddress();

        string[] queryArguments = {
            "SessionID=" + PlayerPrefs.GetString("session_id"),
            "LobbyName=" + lobbyName,
            "UserIP=" + userIP };

        qh = new QueryHandler<loginData>("CreateLobby", queryArguments);

        StartCoroutine(qh.HandleServerRequest());
    }

    private void Update()
    {
        if (qh == null) { return; }

        loginData result;
        qh.RequestDone(out result);
        if (!done)
            HandleResult(result, GameObject.Find("Error").GetComponent<Text>());
    }

    private void HandleResult(loginData result, Text output)
    {
        PlayerPrefs.SetString("session_id", result.SSID);

        if (result.Response == "-1")
        {
            //login action
            done = true;
            CreateServer();
            PlayerPrefs.SetString("LobbyName", lobbyName);
            SceneManager.LoadScene("Game");
        }
        else
        { output.text = result.Response; }
    }

    private void CreateServer()
    {
        GameObject go = new GameObject("ServerBehaviour", typeof(ServerBehaviour));
        DontDestroyOnLoad(go);
        PlayerPrefs.SetInt("player", 1);
    }
}