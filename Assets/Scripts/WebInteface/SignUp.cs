﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SignUp : MonoBehaviour
{
    private string username;
    private string password;
    private string email;
    private string birthDate;
    private QueryHandler<loginData> qh;

    public void OnEnter()
    {
        password = GameObject.Find("Password").transform.GetComponent<InputField>().text;
        username = GameObject.Find("Username").transform.GetComponent<InputField>().text;
        email = GameObject.Find("Email").transform.GetComponent<InputField>().text;
        birthDate = GameObject.Find("Birth date").transform.GetComponent<InputField>().text;

        string[] queryArguments = {"Username="+username,
                                    "Password="+password,
                                    "Email="+email,
                                    "Dateofbirth="+birthDate};

        qh = new QueryHandler<loginData>("SignUp", queryArguments);

        StartCoroutine(qh.HandleServerRequest());
    }

    private void Update()
    {
        if (qh == null) { return; }

        loginData result;
        qh.RequestDone(out result);
        HandleResult(result, GameObject.Find("Error").GetComponent<Text>());
    }

    private void HandleResult(loginData result, Text output)
    {
        if (result.Response == "-1")
        {
            //signup action
            SceneManager.LoadScene("Login");
        }
        else
        {
            output.text = result.Response;
        }
    }
}
