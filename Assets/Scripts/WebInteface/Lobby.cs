﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Lobby : MonoBehaviour
{
    public Text lobbyName;
    public int id;
    public string ip;

    public void SetLobby(int id,string ip, string lobbyName)
    {
        this.lobbyName.text = lobbyName;
        this.id = id;
        this.ip = ip;
    }

    public void ConnectToGame()
    {
        var lobby = gameObject.AddComponent<ConnectToLobby>();
        lobby.Create(this);
    }

    internal void SetLobby(int id, object ip, string lobby_name)
    {
        throw new NotImplementedException();
    }
}
