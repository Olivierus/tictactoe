﻿using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class ConnectToLobby : MonoBehaviour
{
    private QueryHandler<loginData> qh;
    private bool done;
    private Lobby lobby;

    public void Create(Lobby lobby)
    {
        string userIP = NetworkUtilities.GetLocalIPAddress();
        this.lobby = lobby;
        string[] queryArguments = {
            "SessionID=" + PlayerPrefs.GetString("session_id"),
            "LobbyId=" + lobby.id,
            "UserIP=" + userIP };

        qh = new QueryHandler<loginData>("ConnectToLobby", queryArguments);

        StartCoroutine(qh.HandleServerRequest());
    }

    private void Update()
    {
        if (qh == null) { return; }

        loginData result;
        qh.RequestDone(out result);
        if (!done)
            HandleResult(result, GameObject.Find("Error").GetComponent<Text>());
    }

    private void HandleResult(loginData result, Text output)
    {
        PlayerPrefs.SetString("session_id", result.SSID);

        if (result.Response == "-1")
        {
            //login action
            done = true;
            CreateClient();
            SceneManager.LoadScene("Game");
        }
        else
        { output.text = result.Response; }
    }

    private void CreateClient()
    {
        GameObject go = new GameObject("ClientBehaviour", typeof(ClientBehaviour));
        DontDestroyOnLoad(go);
        PlayerPrefs.SetInt("player", 2);
        go.GetComponent<ClientBehaviour>().IP = lobby.ip;
    }
}