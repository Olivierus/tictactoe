﻿using System;
using System.Collections;
using UnityEngine;

public class GetLobbies : MonoBehaviour
{
    private QueryHandler<LobbyData> qh;
    private GameObject lobbiesBoard;
    private GameObject lobbyObject;

    private void Start()
    {
        StartCoroutine(Refresh());
    }

    private IEnumerator Refresh()
    {
        OnEnter();
        yield return new WaitForSeconds(10);
        StartCoroutine(Refresh());
    }

    public void OnEnter()
    {
        string[] queryArguments = { };

        qh = new QueryHandler<LobbyData>("GetAllActiveLobbies", queryArguments);

        StartCoroutine(qh.HandleServerRequest());
        RemoveAll(GameObject.FindObjectsOfType<Lobby>());

        lobbiesBoard = GameObject.Find("LobbiesBoard");
        lobbyObject = (GameObject)Resources.Load("LobbyObject");
    }

    private void RemoveAll(Lobby[] lobbies)
    {
        foreach (var lobby in lobbies)
        {
            Destroy(lobby.gameObject);
        }
    }

    public void Update()
    {
        if (qh == null) { return; }

        LobbyData log;
        if (qh.RequestDone(out log))
        {
            HandleResult(log);
        }
    }

    private void HandleResult(LobbyData result)
    {
        for (int i = 0; i < result.lobbies.Length; i++)
        {
            GameObject currentScoreObject = Instantiate(lobbyObject);
            currentScoreObject.transform.SetParent(lobbiesBoard.transform);
            SetValuesScoreObject(result.lobbies, i, currentScoreObject.GetComponent<Lobby>());
        }
    }

    private void SetValuesScoreObject(LobbyDataSingle[] result, int i, Lobby scoreObject)
    {
        if (i - 1 < result.Length)
            scoreObject.SetLobby(result[i].id, result[i].player1ip, result[i].lobby_name);
    }
}

[System.Serializable]
public class LobbyDataSingle
{
    public int id;
    public string player1ip;
    public string lobby_name;
}

[System.Serializable]
public class LobbyData
{
    public LobbyDataSingle[] lobbies;
}