﻿using UnityEngine;

public class GetScores : MonoBehaviour
{
    private QueryHandler<scoreData> qh;
    private GameObject[] scoreBoards;
    private GameObject scoreObject;

    public void OnEnter()
    {
        string[] queryArguments = { };

        qh = new QueryHandler<scoreData>("gethighscores", queryArguments);

        StartCoroutine(qh.HandleServerRequest());

        scoreBoards = GameObject.FindGameObjectsWithTag("scoreBoard");
        scoreObject = (GameObject)Resources.Load("ScoreObject");
    }

    public void Tick()
    {
        scoreData log;
        if (qh.RequestDone(out log))
        {
            HandleResult(log);
        }
    }

    private void HandleResult(scoreData result)
    {
        for (int i = 1; i < 6; i++)
        {
            GameObject currentScoreObject = Instantiate(scoreObject);
            currentScoreObject.transform.SetParent(scoreBoards[0].transform);
            SetValuesScoreObject(result.highScores, i, currentScoreObject.GetComponent<Score>());
        }

        for (int i = 1; i < 6; i++)
        {
            GameObject currentScoreObject = Instantiate(scoreObject);
            currentScoreObject.transform.SetParent(scoreBoards[1].transform);
            SetValuesScoreObject(result.personalScores, i, currentScoreObject.GetComponent<Score>());
        }
    }

    private void SetValuesScoreObject(scoreDataSingle[] result, int i, Score scoreObject)
    {
        if (i - 1 < result.Length)
            scoreObject.SetScore(i.ToString(), result[i - 1].username, result[i - 1].Score);
        else
            scoreObject.SetScore(i.ToString(), "-", "-");
    }
}

[System.Serializable]
public class scoreDataSingle
{
    public string Score;
    public string username;
}

[System.Serializable]
public class scoreData
{
    public scoreDataSingle[] highScores;
    public scoreDataSingle[] personalScores;
}