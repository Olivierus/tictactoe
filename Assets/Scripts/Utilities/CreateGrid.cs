﻿using System;
using UnityEngine;

public class CreateGrid : MonoBehaviour
{
    public GameObject[] Generate(GameObject gridElement, Vector2 gridSize, Rect gridLocation, float margin = 0)
    {
        GameObject[] grid = new GameObject[(int)gridSize.x * (int)gridSize.y];
        int row = (int)gridSize.x;
        int column = (int)gridSize.y;

        for (int x = 0; x < row; x++)
        {
            for (int y = 0; y < column; y++)
            {
                Vector3 position = GridElementPosition(gridLocation, gridSize, new Vector2(x, y));
                GameObject field;

                if (gridElement.GetComponent<RectTransform>())
                {
                    field = Instantiate(gridElement, Vector3.zero, Quaternion.identity, transform);
                    RectTransform fieldSize = field.GetComponent<RectTransform>();
                    fieldSize.anchoredPosition3D = position;
                    ResizeUI(ref field, gridSize, gridLocation, margin);
                }
                else
                {
                    field = Instantiate(gridElement, position, Quaternion.identity, transform);
                    ResizeGameObject(ref field, gridSize, gridLocation, margin);
                }

                field.name = gridElement.name;

                grid[column * x + y] = field;
            }
        }
        return grid;
    }

    private Vector3 GridElementPosition(Rect gridLocation, Vector2 gridSize, Vector2 index)
    {
        Vector2 gridOrgin = GetGridOrigin(gridLocation);

        float widthObject = gridLocation.width / gridSize.x;
        float heightObject = gridLocation.height / gridSize.y;

        float xObject = gridOrgin.x;
        float yObject = gridOrgin.y;

        xObject -= widthObject * index.x + (widthObject / 2);
        yObject += heightObject * index.y + (heightObject / 2);

        return new Vector3(xObject, yObject, Camera.main.nearClipPlane);
    }

    private Vector2 GetGridOrigin(Rect gridLocation)
    {
        Vector2 orgin = new Vector2();

        orgin.x = gridLocation.xMax;
        orgin.y = gridLocation.yMin;

        return orgin;
    }

    private void ResizeUI(ref GameObject field, Vector2 gridSize, Rect gridLocation, float margin)
    {
        Rect currentSize = field.GetComponent<RectTransform>().rect;

        float width = (gridLocation.width / gridSize.x) - margin;
        float height = (gridLocation.height / gridSize.y) - margin;

        RectTransform fieldSize = field.GetComponent<RectTransform>();
        fieldSize.sizeDelta = new Vector2(width, height);
    }

    private void ResizeGameObject(ref GameObject field, Vector2 gridSize, Rect gridLocation, float margin)
    {
        float size;
        if (gridLocation.width < gridLocation.height)
        {
            size = gridLocation.width;
        }
        else
        {
            size = gridLocation.height;
        }
        size -= margin;
        field.transform.localScale = new Vector3(size / gridSize.x, size / gridSize.y, 0);
    }
}
