﻿using System.Text;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class QueryHandler<T>
{
    // private const string mainLocation = "http://localhost/GameBoxWebInterface/";
    private const string mainLocation = "https://studenthome.hku.nl/~bas.dijkstra/GameBoxWebInterface/";
    private string location;
    private string[] arguments;
    private T response;
    private bool done = false;

    public QueryHandler(string location, string[] arguments)
    {
        this.location = location;
        this.arguments = arguments;
    }

    public bool RequestDone(out T response)
    {
        response = this.response;

        if (done)
        {
            done = false;
            return true;
        }
        else
        {
            return false;
        }
    }

    public IEnumerator HandleServerRequest()
    {
        string uri = AdressBuilder();
        Debug.Log(uri);

        using (UnityWebRequest www = UnityWebRequest.Get(uri))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                foreach (System.Collections.Generic.KeyValuePair<string, string> dict in www.GetResponseHeaders())
                {
                    sb.Append(dict.Key).Append(": \t[").Append(dict.Value).Append("]\n");
                }

                string result = www.downloadHandler.text;
                if (result != "")
                {
                    ServerResponsFromJson(result);
                }
            }
        }
    }

    private string AdressBuilder()
    {
        List<string> allArguments = new List<string>();
        allArguments.AddRange(arguments);
        if (PlayerPrefs.HasKey("session_id"))
        {
            allArguments.Add("SessionID=" + PlayerPrefs.GetString("session_id"));
        }

        string url = mainLocation + location + ".php";
        string uri = url + "?";

        foreach (string argument in arguments)
        {
            uri += argument + "&";
        }
        uri = uri.Remove(uri.Length - 1);

        return uri;
    }

    private void ServerResponsFromJson(string response)
    {
        try
        {
            Debug.Log(response);
            T data;
            data = JsonUtility.FromJson<T>(response);
            this.response = data;
            done = true;
        }
        catch
        {
            Debug.LogWarning("Server down");
        }
    }
}

