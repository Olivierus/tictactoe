﻿using System;
using System.Net;
using System.Net.Sockets;

public static class NetworkUtilities
{
    public static string GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                return ip.ToString();
            }
        }
        throw new Exception("No network adapters with an IPv4 address in the system!");
    }

    public static void OpenPortInFirewall()
    {
        string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
        string command = "/C netsh advfirewall firewall add rule name=" + "TicTacToe" + " dir=in action=allow edge=yes remoteip=any protocol=UDP localport=9000 program=" + path;
        RunCommand(command);
    }

    public static void ClosePortInFirewall()
    {
        string command = "/C netsh advfirewall firewall Delete rule name=" + "TicTacToe";
        RunCommand(command);
    }

    private static void RunCommand(string command)
    {
        System.Diagnostics.Process process = new System.Diagnostics.Process();
        System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
        startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
        startInfo.FileName = "cmd.exe";
        startInfo.Arguments = command;
        startInfo.Verb = "runas";
        process.StartInfo = startInfo;
        process.Start();
    }
}
