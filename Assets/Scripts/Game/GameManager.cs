﻿using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public bool server = false;

    private PingPong pingPong;

    public void StartGame(bool server)
    {
        pingPong = new PingPong(server);

        GameObject game = (GameObject)Resources.Load("Game");
        gameObject.AddComponent<RemoveLobby>();

        Instantiate(game);

        if (!server) { return; }

        PlayerPrefs.SetInt("turn", 1);

        this.server = server;
    }

    public void GameOver(object data)
    {
        StartCoroutine(ShowWinner((int)data));
    }

    IEnumerator ShowWinner(int playerId)
    {
        if (playerId != -1)
        {
            Debug.LogWarning("Player " + playerId + " wins!");
            GameObject.Find("Winner").GetComponent<Text>().text = "Player " + playerId + " wins!";
        }
        else
        {
            Debug.LogWarning("Tie!");
            GameObject.Find("Winner").GetComponent<Text>().text = "Tie!";
        }

        yield return new WaitForSeconds(4);
        gameObject.GetComponent<RemoveLobby>().DeactivatedLobby();
        SceneManager.LoadScene("Lobby");
    }
}