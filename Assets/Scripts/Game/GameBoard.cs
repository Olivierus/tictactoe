﻿using System.Linq;
using UnityEngine;

public class GameBoard : MonoBehaviour
{
    public int gridSize;
    public GameObject gridObject;
    public float margin = 10f;

    public Field[,] board;
    public Sprite[] visuals;

    private GameObject[] currentGrid;
    private EventBin updateBoard;
    private EventBin gameOver;

    private const int row = 3;
    private int playerId = 0;

    private void Start()
    {
        CreateFields();
        visuals = Resources.LoadAll("Fields", typeof(Sprite)).Cast<Sprite>().ToArray();
        InitFields();

        playerId = PlayerPrefs.GetInt("player");
        updateBoard = new EventBin(UpdateBoard);
        gameOver = new EventBin(FindObjectOfType<GameManager>().GameOver);
    }

    private void CreateFields()
    {
        CreateGrid grid = gameObject.AddComponent<CreateGrid>();
        Rect canvasRect = ViewPortToWorld(Camera.main);
        currentGrid = grid.Generate(gridObject, new Vector2(gridSize, gridSize), canvasRect, margin);
        Destroy(grid);
    }

    private Rect ViewPortToWorld(Camera main)
    {
        Vector3 leftBottomCorner = main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Vector3 rightTopCorner = main.ViewportToWorldPoint(new Vector3(1, 1, 0));

        float w = Mathf.Abs(leftBottomCorner.x) + Mathf.Abs(rightTopCorner.x);
        float h = Mathf.Abs(leftBottomCorner.y) + Mathf.Abs(rightTopCorner.y);

        Vector3 pos = leftBottomCorner;
        if (w < h)
        {
            pos.y += (h - w) / 2;
            h = w;
        }
        else
        {
            pos.x += (w - h) / 2;
            w = h;
        }

        return new Rect(pos.x, pos.y, w, h);
    }

    private void InitFields()
    {
        board = new Field[gridSize, gridSize];
        for (int x = 0; x < gridSize; x++)
        {
            for (int y = 0; y < gridSize; y++)
            {
                int index = gridSize * x + y;
                board[x, y] = currentGrid[index].AddComponent<Field>();
                board[x, y].board = this;
                board[x, y].data.x = x;
                board[x, y].data.y = y;
            }
        }
    }

    public void UpdateBoard(FieldData data)
    {
        PlayerPrefs.SetInt("turn", 0);
        updateBoard.SendEvent(data);
        CheckBoard(data.x, data.y, playerId);
    }

    private void UpdateBoard(object data)
    {
        PlayerPrefs.SetInt("turn", 1);
        FieldData field = (FieldData)data;
        board[field.x, field.y].ChangeField(field.Value);
        CheckBoard(field.x, field.y, playerId);
    }

    private void CheckBoard(int x, int y, int value)
    {
        CheckColumn(x, value);

        CheckRow(y, value);

        CheckDiagonal(x, y, value);

        CheckReverseDiagonal(x, y, value);

        CheckDraw();
    }

    private void CheckDraw()
    {
        int fieldsChecked = 0;
        for (int i = 0; i < gridSize; i++)
        {
            for (int j = 0; j < gridSize; j++)
            {
                if (board[i, j].data.Value == 0)
                    continue;
                else
                    fieldsChecked++;
            }
        }

        if (fieldsChecked == gridSize * 2)
        {
            gameOver.SendEvent(-1);
            FindObjectOfType<GameManager>().GameOver(-1);
        }
    }

    private void CheckReverseDiagonal(int x, int y, int value)
    {
        if (x + y == row - 1)
        {
            for (int i = 0; i < row; i++)
            {
                if (board[i, (row - 1) - i].data.Value != value)
                    break;
                if (i == row - 1)
                {
                    gameOver.SendEvent(playerId);
                    FindObjectOfType<GameManager>().GameOver(playerId);
                }
            }
        }
    }

    private void CheckDiagonal(int x, int y, int value)
    {
        if (x == y)
        {
            for (int i = 0; i < row; i++)
            {
                if (board[i, i].data.Value != value)
                    break;
                if (i == row - 1)
                {
                    gameOver.SendEvent(playerId);
                    FindObjectOfType<GameManager>().GameOver(playerId);
                }
            }
        }
    }

    private void CheckRow(int y, int value)
    {
        for (int i = 0; i < row; i++)
        {
            if (board[i, y].data.Value != value)
                break;
            if (i == row - 1)
            {
                gameOver.SendEvent(playerId);
                FindObjectOfType<GameManager>().GameOver(playerId);
            }
        }
    }

    private void CheckColumn(int x, int value)
    {
        for (int i = 0; i < row; i++)
        {
            if (board[x, i].data.Value != value)
                break;

            if (i == row - 1)
            {
                gameOver.SendEvent(playerId);
                FindObjectOfType<GameManager>().GameOver(playerId);
            }
        }
    }
}
