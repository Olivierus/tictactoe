[System.Serializable]
public class Packet
{
    public string eventName;
    public object data;

    public Packet(string v, object data)
    {
        this.eventName = v;
        this.data = data;
    }
}