﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class EventManager : MonoBehaviour
{

    private Dictionary<string, Action<object>> eventDictionary;

    private static EventManager eventManager;

    public static EventManager instance
    {
        get
        {
            if (eventManager == null)
            {
                eventManager = GameObject.FindObjectOfType<EventManager>();

                if (eventManager == null)
                {
                    GameObject container = new GameObject("EventManager");
                    eventManager = container.AddComponent<EventManager>();
                }
            }
            return eventManager;
        }
    }

    private void Awake()
    {
        eventManager = this;

        if (eventDictionary == null)
        {
            eventDictionary = new Dictionary<string, Action<object>>();
        }
    }

    public static void StartListening(string eventName, Action<object> listener)
    {
        Action<object> thisEvent;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent += listener;
            instance.eventDictionary[eventName] = thisEvent;
        }
        else
        {
            thisEvent += listener;
            instance.eventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void TriggerEvent(string eventName, object eventParam)
    {
        Action<object> thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke(eventParam);
        }
    }
}