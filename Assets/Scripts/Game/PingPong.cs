using UnityEngine.SceneManagement;
using System.Timers;
using UnityEngine;

public class PingPong
{
    private static EventBin ping;
    private static int response;
    private static Timer responseTimer;
    private static int tries = 0;

    public PingPong(bool server)
    {
        ping = new EventBin(Ping);
        SetTimer();

        Debug.Log(server);

        if (server)
            ping.SendEvent(1);
    }

    private void Ping(object data)
    {
        response = (int)data;
    }

    private static void SetTimer()
    {
        responseTimer = new Timer();
        responseTimer.Interval = 2000;
        responseTimer.Elapsed += (sender, e) => CheckResponse();
        responseTimer.AutoReset = true;
        responseTimer.Enabled = true;
    }

    private static void CheckResponse()
    {
        Debug.Log(response);
        responseTimer.Stop();
        if (response == 1)
        {
            ping.SendEvent(1);
            response = 0;
            Debug.Log("Ping");
        }
        else
        {
            tries++;
            if (tries > 10)
            {
                SceneManager.LoadScene("Lobby");
            }
        }
        responseTimer.Start();
    }
}