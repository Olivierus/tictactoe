using System;

[Serializable]
public struct FieldData
{
    public int x;
    public int y;
    public int Value;
}