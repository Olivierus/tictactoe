﻿using UnityEngine;

public class InputSystem : MonoBehaviour
{
    private int player;

    private void Start()
    {
        player = PlayerPrefs.GetInt("player");
    }

    private void Update()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(mousePosition, Vector2.up);

        if (hit.collider != null)
        {
            if (Input.GetMouseButtonDown(0) && PlayerPrefs.GetInt("turn") == 1)
            {
                hit.collider.GetComponent<Field>().value = player;
            }
        }
    }
}