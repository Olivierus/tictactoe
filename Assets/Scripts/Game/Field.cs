﻿using System;
using UnityEngine;

[Serializable]
public class Field : MonoBehaviour
{
    public FieldData data;
    public int value
    {
        get { return data.Value; }
        set
        {
            data.Value = value;
            UpdateField(data.Value);
        }
    }
    public GameBoard board;

    private bool played = false;
    private SpriteRenderer imageRenderer;
    private Sprite[] visuals;

    private void Innit()
    {
        imageRenderer = GetComponent<SpriteRenderer>();
    }

    private void UpdateField(int value)
    {
        ChangeField(value);
        board.UpdateBoard(data);
        played = true;
    }

    public void ChangeField(int value)
    {
        if (!imageRenderer) { Innit(); }

        if (imageRenderer.sprite == board.visuals[value]) { return; }

        if (played) { return; }

        imageRenderer.sprite = board.visuals[value];
    }
}
