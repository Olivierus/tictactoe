using System;

[Serializable]
public class EventBin
{
    private Action<object> eventMethod;
    private string eventName;

    public EventBin(Action<object> eventMethod)
    {
        this.eventMethod = eventMethod;
        this.eventName = eventMethod.Method.Name;
        EventManager.StartListening(eventName, eventMethod);
    }

    public void SendEvent(object data = default)
    {
        Packet packet = new Packet(eventName, data);
        Connection.instance.allData.Add(packet);
    }
}